﻿#include <iostream>
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")
#include <ws2tcpip.h>
#include <vector>
#include <fstream>
#include <thread>
#include <mutex>
#include <string>
#include <mysql.h>
#pragma comment(lib,"libmysql.lib")

std::mutex file_lock;
std::mutex countThread_lock;
unsigned long countThread = 0;

void loging(std::string str) {
	file_lock.lock();
	std::ofstream file("error.log", std::ios::app);
	file << str << "\n";
	file.close();
	file_lock.unlock();
}

void toUtf8(std::string& str) {
	std::string tmp = "";
	for (size_t i = 0; i < str.size(); i++) {
		if (str[i] >= -16 && str[i] < 0) {
			tmp += -47;
			str[i] -= 112;
		}
		else if (str[i] < 0) {
			tmp += -48;
			str[i] -= 48;
		}
		tmp += str[i];
	}
	str = tmp;
}

bool isUtf8(std::string& str) {
	std::string res = "";
	for (size_t i = 0; i < str.size(); i++)
	{
		if (str[i] == (char)0xD0 && (str[++i] >= (char)0x90 && str[i] <= (char)0xBF)) {
			return true;
		}
		else if (str[i] == (char)0xD1 && (str[++i] >= (char)0x80 && str[i] <= (char)0x8F)) {
			return true;
		}
		if (str[i] > -64 && (str[i] < 0 || str[i] > 45) && str[i] != 47)res += str[i];
	}
	str = res;
	return false;
}

std::string getContent(const std::string& forFind, std::string begin, std::string end, bool encoding = true) {
	std::string res = "";
	size_t findBegin = forFind.find(begin);
	if (findBegin != forFind.npos) {
		size_t findEnd = forFind.find(end, findBegin + begin.size());
		std::string strTmp = "";
		if (findEnd != forFind.npos) {
			strTmp += forFind.substr(findBegin + begin.size(), findEnd - (findBegin + begin.size()));
			if (encoding) {
				if (isUtf8(strTmp)) {
					for (size_t i = 0; i < strTmp.size(); i++) {
						if ((strTmp[i] == (char)0xD0 && (strTmp[i + 1] >= (char)0x90 && strTmp[i + 1] <= (char)0xBF)) ||
							(strTmp[i] == (char)0xD1 && (strTmp[i + 1] >= (char)0x80 && strTmp[i + 1] <= (char)0x8F))) {
							res += strTmp[i];
							res += strTmp[++i];
						}
						else if (!(strTmp[i] == (char)0xD0 || strTmp[i] == (char)0xD1 || strTmp[i] == (char)0xC2 || strTmp[i] == (char)0xE2 || strTmp[i] == (char)0xC3) &&
							(strTmp[i] > -64 && (strTmp[i] < 0 || strTmp[i] > 45) && strTmp[i] != 47))res += strTmp[i];
					}
					return res;
				}
				else {
					toUtf8(strTmp);
					return strTmp;
				}
			}
			else {
				return strTmp;
			}
		}
	}
	return res;
}

void getHttp(std::string url, std::string& httpAnswer) {
	addrinfo* ipAdress;
	addrinfo hints = {};
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	if (getaddrinfo(url.c_str(), "80", &hints, &ipAdress) != 0) {
		ipAdress = nullptr;
		return;
	}
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) < 0) {
		std::cout << "Error load library\n";
		return;
	}
	SOCKET connection = socket(ipAdress->ai_family, ipAdress->ai_socktype, IPPROTO_TCP);
	if (connect(connection, ipAdress->ai_addr, (int)ipAdress->ai_addrlen) < 0) {
		closesocket(connection);
		WSACleanup();
		ipAdress = nullptr;
		return;
	}
	std::string get = "GET / HTTP/1.1\r\nHost: " + url + "\r\nConnection: close\r\n\r\n";
	if (send(connection, get.c_str(), (int)get.length(), 0) < 0) {
		closesocket(connection);
		WSACleanup();
		ipAdress = nullptr;
		return;
	}
	char* out = new char[MAXWORD];
	short lenHttpAnswer = -1;
	bool mode = true;
	ioctlsocket(connection, FIONBIO, (unsigned long*)&mode);
	int error = 0;
	int countErr = 0;
	size_t timeout = 1000000;
	while (lenHttpAnswer != 0 && countErr < timeout) {
		do {
			lenHttpAnswer = recv(connection, out, MAXWORD - 1, 0);
			error = WSAGetLastError();
			std::this_thread::yield();
		} while (error == WSAEWOULDBLOCK && ++countErr < timeout);
		if (lenHttpAnswer > 0) {
			out[lenHttpAnswer] = '\0';
			httpAnswer += out;
		}
	}
	delete[] out;
	out = nullptr;
	shutdown(connection, 2);
	closesocket(connection);
	WSACleanup();
}

bool threadWork(std::string url, std::string proto = "http://") {
	auto runThread = [](bool var) {
		countThread_lock.lock();
		if (var) {
			countThread++;
		}
		else countThread--;
		countThread_lock.unlock();
	};
	runThread(true);
	MYSQL mysql;
	mysql_init(&mysql);
	if (mysql_real_connect(&mysql, "127.0.0.1", "root", "root", "searcher", 3306, 0, 0) == NULL) {
		mysql_close(&mysql);
		runThread(false);
		return false;
	}
	std::string select = "SELECT url FROM indexru where url=\"" + url + "\"";
	mysql_query(&mysql, select.c_str());
	MYSQL_RES* result = mysql_store_result(&mysql);
	auto closeAll = [&result, &mysql]() {
		result = nullptr;
		mysql_close(&mysql);
	};
	if (result->row_count == 0) {
		std::string httpAnswer = "";
		getHttp(url, httpAnswer);
		if (httpAnswer != "") {
			size_t findCode = httpAnswer.find("HTTP/1.1");
			if (findCode != httpAnswer.npos) {
				std::string httpCode = httpAnswer.substr(findCode + 9, 3);
				if (httpCode == "200") {
					std::string title = getContent(httpAnswer, "<meta name=\"keywords\" content=", ">");
					if (title != "") {
						std::string insert = "INSERT INTO indexRu(url, keywords) VALUES(\"" + proto + url + "\",\"" + title + "\")";
						if (mysql_query(&mysql, insert.c_str()) != 0) {
							loging(url + "\n" + title + "\n" + mysql_error(&mysql));
						}
					}
					closeAll();
					runThread(false);
					return true;
				}
				else {
					std::string location = getContent(httpAnswer, "Location:", "\r\n",false);
					if (location != "" && location.find("https") == location.npos) {
						std::string newUrl = getContent(location, "//", "/", false);
						if (newUrl != url && newUrl != "") {
							std::thread queryT(threadWork, newUrl);
							queryT.detach();
						}
					}
				}
			}
		}
	}
	closeAll();
	runThread(false);
	return false;
}

void bruteforceDns() {
	std::string query = "";
	int i = (int)query.size() - 1;
	while (query.size() <= 3) //количество символов имени хоста
	{
		if (i < 0) {
			query.insert(query.begin(), 'a' - 1);
			i++;
			continue;
		}
		else if (query[i] == 'z') {
			query[i] = '0' - 1;
			continue;
		}
		else if (query[i] == '9' && !(query[0] == '9' || query[query.size() - 1] == '9')) {
			query[i] = '-' - 1;
			continue;
		}
		else if ((query[i] == '9' && (query[0] == '9' || query[query.size() - 1] == '9')) || query[i] == '-') {
			query[i] = 'a';
			i--;
			continue;
		}
		else {
			query[i]++;
			i = (int)query.size() - 1;
		}
		while (countThread > 100) { //максимальное колличество потоков
			std::this_thread::yield();
		}
		std::this_thread::yield();
		std::thread queryT(threadWork, query + ".ru");
		queryT.detach();
		//threadWork(query + ".ru");
	}
}

void queryFromFile(std::string fileName) {
	std::ifstream listHost(fileName);
	while (!listHost.eof())
	{
		std::string host;
		listHost >> host;
		while (countThread > 100) { //максимальное колличество потоков
			std::this_thread::yield();
		}
		std::thread fromFile(threadWork, host);
		fromFile.detach();
		//threadWork(host);
	}
	listHost.close();
}

bool createDB() {
	MYSQL mysql;
	mysql_init(&mysql);
	if (mysql_real_connect(&mysql, "127.0.0.1", "root", "root", "searcher", 3306, 0, 0) == NULL) {
		std::string err = mysql_error(&mysql);
		if (err.find("Unknown database") != err.npos) {
			mysql_close(&mysql);
			mysql_real_connect(&mysql, "127.0.0.1", "root", "root", NULL, 3306, 0, 0);
			if (mysql_query(&mysql, "CREATE DATABASE searcher CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci") != 0) {
				loging(mysql_error(&mysql));
				mysql_close(&mysql);
				return false;
			}
			else {
				if (mysql_query(&mysql, "use searcher") != 0) {
					loging(mysql_error(&mysql));
					mysql_close(&mysql);
					return false;
				}
			}
		}
		else {
			std::cout << mysql_error(&mysql) << std::endl;
			mysql_close(&mysql);
			return false;
		}
	}
	mysql_query(&mysql, "SHOW TABLES where Tables_in_searcher = \"indexRu\"");
	MYSQL_RES* result = mysql_store_result(&mysql);
	if (result->row_count == 0) {
		if (mysql_query(&mysql, "CREATE TABLE indexRu(url VARCHAR(20), keywords TEXT)") != 0) {
			std::string error = mysql_error(&mysql);
			loging(error);
			result = nullptr;
			mysql_close(&mysql);
			return false;
		}
	}
	result = nullptr;
	mysql_close(&mysql);
	return true;
}

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");
	if (!createDB()) return -1;

	if (argc <= 1) {
		bruteforceDns();
	}
	else {
		queryFromFile(argv[1]);
	}
	while (countThread != 0) {
		std::this_thread::yield();
	}
}
