﻿#include <iostream>
#include <string>
#include <sstream>
#include <mysql.h>
#pragma comment(lib,"libmysql.lib")
#include <vector>
#include <map>
#include <algorithm>

void toUtf8(std::string& str) {
	std::string tmp = "";
	for (size_t i = 0; i < str.size(); i++) {
		if (str[i] > -33 && str[i] < 0) {
			tmp += -47;
			str[i] -= 96;
		}
		else if (str[i] < 0) {
			tmp += -48;
			str[i] += 16;
		}
		tmp += str[i];
	}
	str = tmp;
}

int main()
{
	setlocale(LC_ALL, "rus");
	std::string query = "";
	std::cout << "searcher: ";
	std::getline(std::cin, query);
	std::stringstream ss = {};
	ss << query;
	std::string keyword = "";
	std::map<std::string, int> resultIndex = {};
	std::vector<std::pair<std::string, int>> result = {};

	MYSQL mysql;
	mysql_init(&mysql);
	if (mysql_real_connect(&mysql, "127.0.0.1", "root", "root", "searcher", 3306, 0, 0) == NULL) {
		std::cout << mysql_error(&mysql) << "\n";
		mysql_close(&mysql);
		return false;
	}

	while (!ss.eof())
	{
		ss >> keyword;
		toUtf8(keyword);
		std::string select = "SELECT url FROM indexru where keywords LIKE \"%" + keyword + "%\"";
		mysql_query(&mysql, select.c_str());
		MYSQL_RES* resultSelect = mysql_store_result(&mysql);
		MYSQL_ROW row = mysql_fetch_row(resultSelect);
		while (row != NULL)
		{
			if (resultIndex.find(row[0]) == resultIndex.end()) {
				resultIndex[row[0]] = 1;
			}
			else {
				resultIndex[row[0]] += 1;
			}
			row = mysql_fetch_row(resultSelect);
		}

	}
	mysql_close(&mysql);
	for (std::pair<std::string,int> el : resultIndex) {
		result.push_back(el);
	}
	std::sort(result.begin(), result.end(), [](const auto& a, const auto& b) { return a.second > b.second; });
	for (std::pair<std::string, int> el : result) {
		std::cout << "http://" << el.first << "\n";
	}

}


